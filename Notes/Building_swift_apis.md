# Building the Swift Apis

Swift for TensorFlow has a dependency on the swift-apis found [here](https://github.com/tensorflow/swift-apis). This document details
the challenges encountered with building swift-apis with modern Swift Toolchains > 5.5.

## PythonKit Dependency

Swift-apis is built using `cmake` and has several dependencies. One dependency is Swift PythonKit. Apparently, Swift PythonKit was build using
`cmake` before (see [CMake file](https://github.com/tensorflow/swift-apis/blob/f51ee4618d652a2419e998bf9418ad80bda67454/CMakeLists.txt#L114) ), but the newer versions seem to be build using swift package manager, as a `CMakeLists.txt` is missing, see [PythonKit](https://github.com/philipturner/PythonKit).
Hence the following error:

```code
#14 2.475 FAILED: python-kit-prefix/src/python-kit-stamp/python-kit-configure /out/python-kit-prefix/src/python-kit-stamp/python-kit-configure
#14 2.475 cd /out/python-kit-prefix/src/python-kit-build && /usr/bin/cmake -D BUILD_SHARED_LIBS=YES -D CMAKE_BUILD_TYPE=Release -D CMAKE_MAKE_PROGRAM=/usr/bin/ninja -D CMAKE_Swift_COMPILER=/usr/bin/swiftc -D CMAKE_Swift_COMPILER_TARGET= -D CMAKE_Swift_FLAGS= -GNinja /out/python-kit-prefix/src/python-kit && /usr/bin/cmake -E touch /out/python-kit-prefix/src/python-kit-stamp/python-kit-configure
#14 2.475 CMake Error: The source directory "/out/python-kit-prefix/src/python-kit" does not appear to contain CMakeLists.txt.
```

It seems swift-apis depends on PythonKit [v0.1.0](https://github.com/pvieito/PythonKit/tree/v0.1.0), as this version still has a CMakeLists file.
After v0.1.0, this CMakeLists file disappears, see [v0.1.1](https://github.com/pvieito/PythonKit/tree/v0.1.1).

To fix it we will need to patch the CMakeLists.txt of the swift-apis git repo, either to use v0.1.0 or to use a more modern version.

~~Another option would be to turn off building with PythonKit for now:~~

```code
RUN cmake -B out -G Ninja -S swift-apis -D CMAKE_BUILD_TYPE=Release -D ENABLE_PYTHON_SUPPORT=OFF&& \
    cmake --build out
```

It looks like swift-apis needs Python numpy to build. This has been installed using pip, but cannot be found:

```code
=> => # ModuleNotFoundError: No module named 'numpy'
 => => # Is numpy installed?
 => => # INFO: Elapsed time: 15.964s
 => => # INFO: 0 processes.
 => => # FAILED: Build did NOT complete successfully (11 packages loaded, 16 targets configured)
 => => # FAILED: Build did NOT complete successfully (11 packages loaded, 16 targets configured)
```

This depends on whether Python 2.x or Python 3.x is needed to build swift-apis. Numpy is found for Python 2.x but
not installed for Python 3.x
