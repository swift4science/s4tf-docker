# Bazel

## Python 3

Ubuntu 18.04 still include python 2.7. Make sure Bazel uses python3 for the build and that the two python versions are not mixed.

```code
RUN  update-alternatives --install /usr/bin/python python /usr/bin/python3.6 20
```

Seems to solve this problem.

## Socket close error

Ran into this error:

```code
#20 373.8 Server terminated abruptly (error code: 14, error message: 'Socket closed', log file: '/s4tf/caches/bazel/tensorflow/662cbabc25959c2fd8293a763eebc0de/server/jvm.out')
```

This seem to be a TensorFlow issue:

- https://github.com/tensorflow/tensorflow/issues/41480
- https://github.com/tensorflow/models/issues/3647

The second post suggest to change the -j option to a lower number of processors to prevent memory from running out.
