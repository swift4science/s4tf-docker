# s4tf-docker

A docker building Swift for TensorFlow with the latest version of Swift

# To build the docker

Run:

```code
chmod +x ./build.sh
./build.sh
```

Or:

```code
docker build . -t swift4science/s4tf-docker
```

## More information about building Swift for TensorFlow

To build Swift for Tensorflow with modern Swift >= 5.5, some challenges have to be overcome.
The following documents describe the specific challenges per dependency:

- [Building Swift Apis](./Notes/Building_swift_apis.md)

# To run the Docker

Run:

```code
chmod +x ./run.sh
./run.sh
```

Or:

```code
docker run --rm -it --name s4tf_test swift4science/s4tf-docker /bin/bash
```

This will run the docker and open a terminal that can be used to interact with Swift.
