#/bin/bash
docker build . -t swift4science/s4tf-docker --build-arg build_s4tf=true --build-arg bazel_version=4.2.1 --build-arg tensorflow_branch=r2.8 --build-arg tensorflow_version=2.8.0 