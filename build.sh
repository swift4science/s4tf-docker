#/bin/bash
docker build . -t swift4science/s4tf-docker --build-arg build_s4tf=true --build-arg bazel_version=3.1.0 --build-arg tensorflow_branch=r2.4 --build-arg tensorflow_version=2.4.4 