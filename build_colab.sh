
export build_s4tf=true
export bazel_version=4.2.1
export tensorflow_branch=r2.8
export tensorflow_version=2.8.0
export DEBIAN_FRONTEND=noninteractive
export tensorflow_march=native

# Most from https://gist.github.com/philipturner/b155095efbc2e9669cf884181a98e560

# Install build dependencies, from: https://github.com/philipturner/tensorflow-swift/blob/main/utils/install-ubuntu1804.sh
apt update && apt upgrade -y && \ 
apt-get install -y --no-install-recommends \
    clang \        
    build-essential \
    ca-certificates \
    curl \
    git \
    python3-dev \
    python3-tk \
    python3-pip \
    clang \
    libcurl4-openssl-dev \
    libicu-dev \
    libpython-dev \
    libpython3-dev \
    libncurses5-dev \
    libxml2 \
    libblocksruntime-dev \
    gnupg2
    
# Add bazel and cmake repositories. From: https://github.com/philipturner/swift-for-tensorflow/blob/main/Dockerfile
curl -qL https://apt.kitware.com/keys/kitware-archive-latest.asc | apt-key add -
echo 'deb https://apt.kitware.com/ubuntu/ bionic main' >> /etc/apt/sources.list

curl -qL https://bazel.build/bazel-release.pub.gpg | apt-key add -
echo 'deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8' >> /etc/apt/sources.list.d/bazel.list        
    
# From: https://github.com/philipturner/swift-for-tensorflow/blob/main/Dockerfile
apt update -y && \
apt upgrade -y && \
apt install -y cmake ninja-build python3-setuptools \
    bazel-${bazel_version} && \
    apt-get clean && \
    rm -rf /tmp/* /var/tmp/* /var/lib/apt/archive/* /var/lib/apt/lists/*


ln -s /usr/bin/bazel-$bazel_version /usr/bin/bazel
pip3 install -U pip six numpy wheel setuptools mock 'future>=0.17.1'   &&  \
# We probably do not need keras, might be needed for some example code? 
pip3 install -U --no-deps keras_applications keras_preprocessing 

# Set python3 as default python
update-alternatives --install /usr/bin/python python /usr/bin/python3.6 20


curl "https://download.swift.org/development/ubuntu1804/swift-DEVELOPMENT-SNAPSHOT-2021-11-12-a/swift-DEVELOPMENT-SNAPSHOT-2021-11-12-a-ubuntu18.04.tar.gz" | tar -xz && \
    mkdir /opt/swift && \
    mv "swift-DEVELOPMENT-SNAPSHOT-2021-11-12-a-ubuntu18.04" "/opt/swift/toolchain"

export PATH="/opt/swift/toolchain/usr/bin:/opt/cmake/bin:$PATH"

mkdir /s4tf 

# This builds but what is it exactly?
# git clone --single-branch -b fan/resurrection https://github.com/ProfFan/swift-apis 
cd /s4tf && git clone https://github.com/philipturner/swift-for-tensorflow

# Download tensorflow binary, disabled for now
#  curl https://storage.googleapis.com/swift-tensorflow-artifacts/oneoff-builds/tensorflow-ubuntu1804-x86.zip --output tensorflow-binary.zip && \
#        mkdir -p /opt/tensorflow && \
#        unzip tensorflow-binary.zip -d /opt/tensorflow

# Try to build tensorflow ourselves

# clone swift-apis
# checkout tensorflow



git clone -b ${tensorflow_branch} --single-branch git://github.com/tensorflow/tensorflow

export S4TF_PATH=/s4tf/swift-for-tensorflow
# Link X10 into the source tree
ln -sf ${S4TF_PATH}/Sources/CX10 /s4tf/tensorflow/swift_bindings && \
ln -sf ${S4TF_PATH}/Sources/x10/xla_client /s4tf/tensorflow/tensorflow/compiler/xla/xla_client && \
ln -sf ${S4TF_PATH}/Sources/x10/xla_tensor /s4tf/tensorflow/tensorflow/compiler/tf2xla/xla_tensor

# ensure that python dependencies are available
#python3 -m pip install --user numpy six
# configure X10/TensorFlow
export USE_DEFAULT_PYTHON_LIB_PATH=1
export TF_NEED_OPENCL_SYCL=0
export TF_DOWNLOAD_CLANG=0
export TF_SET_ANDROID_WORKSPACE=0
export TF_CONFIGURE_IOS=0 
export TF_ENABLE_XLA=1
export TF_NEED_ROCM=0
export TF_NEED_CUDA=0
export TF_CUDA_COMPUTE_CAPABILITIES=7.5
export CC_OPT_FLAGS="-march=${tensorflow_march}" 

export PYTHON_BIN_PATH=$(which python3) && \ 
    python3 ./tensorflow/configure.py && \
    cd tensorflow && \
    bazel --output_user_root /s4tf/caches/bazel/tensorflow build -c opt --define framework_shared_object=false --config short_logs --nocheck_visibility //tensorflow:tensorflow //tensorflow/compiler/tf2xla/xla_tensor:x10 && \
    # terminate bazel daemon
    bazel --output_user_root /s4tf/caches/bazel/tensorflow shutdown 

# # package
export DESTDIR=/s4tf/Library/tensorflow-${tensorflow_version} && \
    mkdir -p ${DESTDIR}/usr/lib && \
    cp /s4tf/tensorflow/bazel-bin/tensorflow/libtensorflow.so.${tensorflow_version} ${DESTDIR}/usr/lib/ && \
    cp /s4tf/tensorflow/bazel-bin/tensorflow/compiler/tf2xla/xla_tensor/libx10.so ${DESTDIR}/usr/lib/ && \
    mkdir -p ${DESTDIR}/usr/include/tensorflow/c && \
    cp /s4tf/tensorflow/tensorflow/c/c_api.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/c_api_experimental.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/c_api_macros.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/tf_attrtype.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/tf_datatype.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/tf_file_statistics.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/tf_status.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/tf_tensor.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    cp /s4tf/tensorflow/tensorflow/c/tf_tstring.h ${DESTDIR}/usr/include/tensorflow/c/ && \
    mkdir -p ${DESTDIR}/usr/include/tensorflow/core/platform && \
    cp /s4tf/tensorflow/tensorflow/core/platform/ctstring.h ${DESTDIR}/usr/include/tensorflow/core/platform/ && \
    cp /s4tf/tensorflow/tensorflow/core/platform/ctstring_internal.h ${DESTDIR}/usr/include/tensorflow/core/platform/ && \
    mkdir -p ${DESTDIR}/usr/include/tensorflow/c/eager && \
    cp /s4tf/tensorflow/tensorflow/c/eager/c_api.h ${DESTDIR}/usr/include/tensorflow/c/eager/ && \
    mkdir -p ${DESTDIR}/usr/include/x10 && \
    # These are not available?
    cp ${S4TF_PATH}/Sources/CX10/device_wrapper.h ${DESTDIR}/usr/include/x10/ && \
    cp ${S4TF_PATH}/Sources/CX10/xla_tensor_tf_ops.h ${DESTDIR}/usr/include/x10/ && \
    cp ${S4TF_PATH}/Sources/CX10/xla_tensor_wrapper.h ${DESTDIR}/usr/include/x10/



# # Build swift-for-tensorflow
export TF_NEED_CUDA=0
export CTEST_OUTPUT_ON_FAILURE=1
export TF_PATH="/s4tf/Library/tensorflow-${tensorflow_version}"
export TF_INCLUDE_PATH="${TF_PATH}/usr/include"
export TF_LIB_PATH="${TF_PATH}/usr/lib"
export LD_LIBRARY_PATH="${TF_PATH}/usr/lib:$LD_LIBRARY_PATH"

if ${build_s4tf} ; then cd ${S4TF_PATH} && \
        swift build --verbose -Xswiftc -DTENSORFLOW_USE_STANDARD_TOOLCHAIN -Xcc -I${TF_INCLUDE_PATH} -Xlinker -L${TF_LIB_PATH} ; fi

if ${build_s4tf} ; then cd ${S4TF_PATH} && \
        swift test --verbose -Xswiftc -DTENSORFLOW_USE_STANDARD_TOOLCHAIN -Xcc -I${TF_INCLUDE_PATH} -Xlinker -L${TF_LIB_PATH} ; fi
