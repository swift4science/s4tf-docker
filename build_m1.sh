#!/bin/bash
# From: https://blog.jaimyn.dev/how-to-build-multi-architecture-docker-images-on-an-m1-mac/
docker buildx build --platform linux/amd64,linux/arm64 --push -t swift4science/s4tf-docker .
